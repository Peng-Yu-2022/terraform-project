resource "aws_security_group" "myapp_sg" {
  name        = "myapp_sg"
  vpc_id      = var.vpc_id
  ingress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = [var.my_ip]
  }
  ingress {
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }
  tags = {
    Name = "${var.env_prefix}-sg"
  }
}

data "aws_ami" "latest-amazon-linux-image" {
  most_recent      = true
  owners           = ["amazon"]
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_instance" "myapp-server" {
  ami = data.aws_ami.latest-amazon-linux-image.id
  instance_type = var.instance_type
  vpc_security_group_ids = [aws_security_group.myapp_sg.id]
  subnet_id = var.subnet_id
  associate_public_ip_address = true
  key_name = "dev-server-key"
  user_data = file("entry-script.sh")
  tags = {
    Name = "${var.env_prefix}-server"
  }
#    connection {
#     type = "ssh"
#     host = self.public_ip
#     user = "ec2_user"
#     private_key = file(var.private_key_location)
#   }
#   provisioner "file" {
#     source = "entry-script.sh"
#     destination = "/home/ec2-user/entry-script.sh"
#   }
#   provisioner "remote-exec" {
#     # inline = [
#     #   "export ENV=dev",
#     #   "mkdir newdir"
#     # ]
#     script = file("entry-script.sh")
#   }

#   provisioner "local-exec" {
#     command = "echo ${self.public_ip} > output.txt"
#   }
}




  
  
  